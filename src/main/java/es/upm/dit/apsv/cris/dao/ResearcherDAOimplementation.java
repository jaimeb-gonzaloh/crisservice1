package es.upm.dit.apsv.cris.dao;

import java.util.List;

import org.hibernate.Session;

import es.upm.dit.apsv.cris.model.Researcher;

import org.hibernate.Session;

public class ResearcherDAOimplementation implements ResearcherDAO {
    
	private static ResearcherDAOimplementation instance = null;
	private ResearcherDAOimplementation() {}
	public static ResearcherDAOimplementation getInstance() {
		if(null==instance)
			instance= new ResearcherDAOimplementation();
		return instance;
	}
	
	@Override
	public Researcher create(Researcher researcher) {
		Session session= SessionFactoryService.get().openSession();
		try {
			session.beginTransaction();
			session.save(researcher);
		    session.getTransaction().commit();
		}catch(Exception e) {
		} finally {session.close();}
		return researcher;
	}

	@Override
	public Researcher read(String researcherId) {
		Session session= SessionFactoryService.get().openSession();
		Researcher r=null;
		try {
			session.beginTransaction();
			r =session.get(Researcher.class, researcherId);
		    session.getTransaction().commit();
		}catch(Exception e) {
		} finally {session.close();}
		return r;
	}

	@Override
	public Researcher update(Researcher researcher) {
		Session session= SessionFactoryService.get().openSession();
		try {
			session.beginTransaction();
			session.saveOrUpdate(researcher);
		    session.getTransaction().commit();
		}catch(Exception e) {
		} finally {session.close();}
		return researcher;
	}

	@Override
	public Researcher delete(Researcher researcher) {
		Session session= SessionFactoryService.get().openSession();
		try {
			session.beginTransaction();
			session.delete(researcher);
		    session.getTransaction().commit();
		}catch(Exception e) {
		} finally {session.close();}
		return researcher;
	}

	@Override
	public List<Researcher> readAll() {
		Session session= SessionFactoryService.get().openSession();
		List<Researcher> l =null;
		try {
			session.beginTransaction();
			l = (List<Researcher>) session.createQuery("from Researcher").getResultList();
		    session.getTransaction().commit();
		}catch(Exception e) {
		} finally {session.close();}
		return l;
	}

	@Override
	public Researcher readByEmail(String email) {
		for (Researcher r : this.readAll())
			if (email.equals(r.getEmail()))
				return r;
		return null;
		
	}

}
